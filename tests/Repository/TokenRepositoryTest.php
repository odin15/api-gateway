<?php

namespace App\Tests\Repository;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Token;
use App\Entity\User;
use App\Repository\TokenRepository;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TokenRepositoryTest extends WebTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    public function testFindOneByTypeAndValue()
    {
        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_ADMIN_AUTHENTICATION);

        $repository = $this->getTokenRepository();
        $found = $repository->findOneByTypeAndValue($token->getType(), $token->getValue());

        $this->assertNotNull($found);
        $this->assertEquals($token->getType(), $found->getType());
        $this->assertEquals($token->getValue(), $found->getValue());
    }

    public function testFindOneByUserAndType()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        $tokens = $user->getTokens();
        $token = $tokens[0];

        $repository = $this->getTokenRepository();
        $found = $repository->findOneByUserAndType($user, $token->getType());

        $this->assertNotNull($found);
        $this->assertEquals($token->getType(), $found->getType());
        $this->assertEquals($token->getValue(), $found->getValue());
    }

    protected function getTokenRepository(): TokenRepository
    {
        return self::getContainer()->get(TokenRepository::class);
    }
}
