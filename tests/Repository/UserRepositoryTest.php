<?php

namespace App\Tests\Repository;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRepositoryTest extends WebTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    public function testUpgradePassword()
    {
        $repository = $this->getUserRepository();

        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        $user = $repository->find($user->getId());
        $newPassword = 'newPassword';

        $repository->upgradePassword($user, $newPassword);

        $found = $repository->find($user->getId());

        $this->assertNotNull($found);
        $this->assertEquals($newPassword, $found->getPassword());
    }

    protected function getUserRepository(): UserRepository
    {
        return self::getContainer()->get(UserRepository::class);
    }
}
