<?php

namespace App\Tests\Event\Listener;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserListenerTest extends WebTestCase
{
    public function testPrePersist()
    {
        $user = (new User())
            ->setEmail('test@test.fr')
            ->setPassword('azerty')
        ;

        $em = self::getContainer()->get(EntityManagerInterface::class);
        $em->persist($user);
        $em->flush();

        $this->assertInstanceOf(\DateTimeInterface::class, $user->getCreatedAt());
    }
}
