<?php

namespace App\Tests\Controller;

use App\Gateway\Forward\GraphQlForward;
use App\Gateway\Forward\RestForward;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class GatewayControllerTest extends WebTestCase
{
    public function testRedirectGraphQl()
    {
        $service = 'customer';
        $version = '1.0';

        $client = static::createClient();

        $graphQlForward = $this->getMockBuilder(GraphQlForward::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $graphQlForward
            ->expects($this->exactly(1))
            ->method('getType')
            ->willReturn(GraphQlForward::TYPE)
        ;
        $graphQlForward
            ->expects($this->exactly(1))
            ->method('forward')
            ->willReturn(new Response())
        ;
        $container = $client->getContainer();
        $container->set('graphql_forward', $graphQlForward);

        $client->request(
            'POST',
            '/'.$service.'/'.$version,
            [],
            [],
            [],
            json_encode([
                'query' => '
                    query test {
                        test
                    }
                ',
                'variables' => [],
            ])
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRedirectGraphQlWithEndingSlash()
    {
        $service = 'customer';
        $version = '1.0';

        $client = static::createClient();

        $graphQlForward = $this->getMockBuilder(GraphQlForward::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $graphQlForward
            ->expects($this->exactly(1))
            ->method('getType')
            ->willReturn(GraphQlForward::TYPE)
        ;
        $graphQlForward
            ->expects($this->exactly(1))
            ->method('forward')
            ->willReturn(new Response())
        ;
        $container = $client->getContainer();
        $container->set('graphql_forward', $graphQlForward);

        $client->request(
            'POST',
            '/'.$service.'/'.$version.'/',
            [],
            [],
            [],
            json_encode([
                'query' => '
                    query test {
                        test
                    }
                ',
                'variables' => [],
            ])
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRedirectRest()
    {
        $service = 'customer';
        $version = '1.0';

        $client = static::createClient();

        $restForward = $this->getMockBuilder(RestForward::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $restForward
            ->expects($this->exactly(1))
            ->method('getType')
            ->willReturn(RestForward::TYPE)
        ;
        $restForward
            ->expects($this->exactly(1))
            ->method('forward')
            ->willReturn(new Response())
        ;
        $container = $client->getContainer();
        $container->set('rest_forward', $restForward);

        $client->request(
            'POST',
            '/'.$service.'/'.$version.'/station/1'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
