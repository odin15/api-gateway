<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LivenessControllerTest extends WebTestCase
{
    public function testAlive()
    {
        $client = static::createClient();

        $client->request('GET', '/alive');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
