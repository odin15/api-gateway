<?php

namespace App\Tests\Security;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Token;
use App\Entity\User;
use App\Security\UserAuthenticator;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserAuthenticatorTest extends WebTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    public function testAuthenticate()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $userAuthenticator = self::getContainer()->get(UserAuthenticator::class);
        /** @var Token $token */
        $token = $userAuthenticator->authenticate($user->getEmail(), UserFixtures::PASSWORD);

        $this->assertNotNull($token);
        $this->assertEquals(Token::TYPE_AUTHENTICATION, $token->getType());
        $this->assertNotNull($token->getValue());
    }

    public function testEncodePassword()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $userAuthenticator = self::getContainer()->get(UserAuthenticator::class);
        $encodedPassword = $userAuthenticator->encodePassword($user, UserFixtures::PASSWORD);

        $this->assertNotEquals(UserFixtures::PASSWORD, $encodedPassword);
    }
}
