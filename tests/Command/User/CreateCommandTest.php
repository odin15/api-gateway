<?php

namespace App\Tests\Command\User;

use App\DataFixtures\UserFixtures;
use App\Entity\User;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateCommandTest extends WebTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            UserFixtures::class,
        ]);
    }

    public function testExecuteWithoutPassword()
    {
        $email = 'test@odeven.fr';

        $application = new Application(self::$kernel);

        $command = $application->find('app:user:create');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'email' => $email,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringNotContainsString('ERROR', $output);
        $this->assertStringContainsString('SUCCESS: User will be created', $output);
        $this->assertStringContainsString('INFO: you should receive an email to confirm your account', $output);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());
    }

    public function testExecuteWithPassword()
    {
        $email = 'test@odeven.fr';
        $password = 'test';

        $application = new Application(self::$kernel);

        $command = $application->find('app:user:create');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'email' => $email,
        ], [
            'password' => $password,
            'roles' => User::ROLE_ADMIN,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringNotContainsString('ERROR', $output);
        $this->assertStringContainsString('SUCCESS: User will be created', $output);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());
    }
}
