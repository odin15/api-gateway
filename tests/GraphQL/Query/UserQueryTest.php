<?php

namespace App\Tests\GraphQL\Query;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Token;
use App\Entity\User;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLArrayWildCard;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLConnectionWildCard;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLTestCase;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLWildCard;

class UserQueryTest extends GraphQLTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            UserFixtures::class,
            TokenFixtures::class,
        ])->getReferenceRepository();
    }

    public function testQueryGetUserById()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQuery('
            query getUser($id: ID!) {
              getUser(id: $id) {
                id,
              }
            }
        ', [
            'data' => [
                'getUser' => [
                    'id' => $user->getId(),
                ],
            ],
        ], [
            'id' => $user->getId(),
        ]);
    }

    public function testQueryGetUserByEmail()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQuery('
            query getUser($email: String!) {
              getUser(email: $email) {
                id,
              }
            }
        ', [
            'data' => [
                'getUser' => [
                    'id' => $user->getId(),
                ],
            ],
        ], [
            'email' => $user->getEmail(),
        ]);
    }

    public function testQueryGetUserByWithoutIdentifier()
    {
        $this->assertQueryWithError('
            query getUser {
              getUser {
                id,
              }
            }
        ');
    }

    public function testQueryGetUserByAuthToken()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_ADMIN_AUTHENTICATION);

        $this->assertQuery('
            query getUserByAuthToken($token: String!) {
              getUserByAuthToken(token: $token) {
                id,
              }
            }
        ', [
            'data' => [
                'getUserByAuthToken' => [
                    'id' => $user->getId(),
                ],
            ],
        ], [
            'token' => $token->getValue(),
        ]);

        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_AUTHENTICATION_TOKEN_EXPIRED);

        $this->assertQueryWithError('
            query getUserByAuthToken($token: String!) {
              getUserByAuthToken(token: $token) {
                id,
              }
            }
        ', [
            'token' => $token->getValue(),
        ],
            'TOKEN_EXPIRED'
        );

        $this->assertQueryWithError('
            query getUserByAuthToken($token: String!) {
              getUserByAuthToken(token: $token) {
                id,
              }
            }
        ', [
            'token' => 'ThisTokenDoesNotExist',
        ],
            'INVALID_TOKEN'
        );
    }

    public function testQueryGetUsers()
    {
        $this->assertQuery('
            query getUsers {
                getUsers {
                    id,
                }
            }
        ', [
            'data' => [
                'getUsers' => new GraphQLArrayWildCard([
                    'id' => new GraphQLWildCard('/^[0-9]+$/'),
                ]),
            ],
        ]);
    }

    public function testQueryGetUsersByIds()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQuery('
            query getUsersByIds ($ids: [ID!]!) {
                getUsersByIds (ids: $ids) {
                    id,
                }
            }
        ', [
            'data' => [
                'getUsersByIds' => new GraphQLArrayWildCard([
                    'id' => new GraphQLWildCard('/^[0-9]+$/'),
                ]),
            ],
        ], [
            'ids' => [
                $user->getId(),
            ],
        ]);
    }

    public function testQuerySearchUsers()
    {
        $this->assertQuery('
            query searchUsers {
                searchUsers {
                    totalCount,
                    edges {
                        cursor
                        node {
                            id
                        }
                    }
                    pageInfo {
                        hasPreviousPage
                        hasNextPage
                    }
                }
            }
        ', [
            'data' => [
                'searchUsers' => new GraphQLConnectionWildCard([
                    'id' => new GraphQLWildCard('/^[0-9]+$/'),
                ]),
            ],
        ]);
    }
}
