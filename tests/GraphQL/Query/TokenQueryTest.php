<?php

namespace App\Tests\GraphQL\Query;

use App\DataFixtures\TokenFixtures;
use App\Entity\Token;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLTestCase;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLWildCard;

class TokenQueryTest extends GraphQLTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
        ])->getReferenceRepository();
    }

    public function testQueryGetToken()
    {
        $this->assertQuery('
            query getToken($userId: ID!, $type: String!) {
              getToken(userId: $userId, type: $type) {
                id,
              }
            }
        ', [
            'data' => [
                'getToken' => [
                    'id' => new GraphQLWildCard('/^[0-9]+$/'),
                ],
            ],
        ], [
            'userId' => 1,
            'type' => Token::TYPE_AUTHENTICATION,
        ]);

        $this->assertQueryWithError('
            query getToken {
              getToken {
                id,
              }
            }
        ');
    }

    public function testQueryIsTokenValid()
    {
        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_ADMIN_AUTHENTICATION);

        $this->assertQuery('
            query isTokenValid($type: String!, $value: String!) {
              isTokenValid(type: $type, value: $value)
            }
        ', [
            'data' => [
                'isTokenValid' => true,
            ],
        ], [
            'type' => Token::TYPE_AUTHENTICATION,
            'value' => $token->getValue(),
        ]);
    }
}
