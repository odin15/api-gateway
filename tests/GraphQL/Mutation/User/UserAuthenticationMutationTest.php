<?php

namespace App\Tests\GraphQL\Mutation\User;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Token;
use App\Entity\User;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLTestCase;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLWildCard;

class UserAuthenticationMutationTest extends GraphQLTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    public function testMutationLogin()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQuery('
            mutation login($input: LoginInput!) {
                login(input: $input) {
                    id,
                    email,
                    token,
                }
            }
        ', [
            'data' => [
                'login' => [
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'token' => new GraphQLWildCard('/^[0-9A-Fa-f]+$/'),
                ],
            ],
        ], [
            'input' => [
                'email' => $user->getEmail(),
                'password' => UserFixtures::PASSWORD,
            ],
        ]);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());
    }

    public function testMutationWithInvalidPassword()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQueryWithError('
            mutation login($input: LoginInput!) {
                login(input: $input) {
                    id,
                    email,
                    token,
                }
            }
        ', [
            'input' => [
                'email' => $user->getEmail(),
                'password' => 'Invalid password',
            ],
        ],
            'BAD_CREDENTIALS'
        );

        $this->assertCount(0, self::getContainer()->get('messenger.transport.messages')->get());
    }

    public function testMutationLogout()
    {
        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_ADMIN_AUTHENTICATION);

        $this->assertQuery('
            mutation logout($input: LogoutInput!) {
                logout(input: $input)
            }
        ', [
            'data' => [
                'logout' => true,
            ],
        ], [
            'input' => [
                'token' => $token->getValue(),
            ],
        ]);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());
    }

    public function testMutationLogoutWithInvalidToken()
    {
        $this->assertQuery('
            mutation logout($input: LogoutInput!) {
                logout(input: $input)
            }
        ', [
            'data' => [
                'logout' => false,
            ],
        ], [
            'input' => [
                'token' => 'InvalidToken',
            ],
        ]);

        $this->assertCount(0, self::getContainer()->get('messenger.transport.messages')->get());
    }
}
