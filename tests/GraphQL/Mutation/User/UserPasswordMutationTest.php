<?php

namespace App\Tests\GraphQL\Mutation\User;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Token;
use App\Entity\User;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLTestCase;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLWildCard;

class UserPasswordMutationTest extends GraphQLTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    public function testMutationResetUserPassword()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_WITH_PASSWORD_FORGOTTEN);

        $this->assertQuery('
            mutation resetUserPassword($input: ResetUserPasswordInput!) {
                resetUserPassword(input: $input)
            }
        ', [
            'data' => [
                'resetUserPassword' => true,
            ],
        ], [
            'input' => [
                'email' => $user->getEmail(),
            ],
        ]);

        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQuery('
            mutation resetUserPassword($input: ResetUserPasswordInput!) {
                resetUserPassword(input: $input)
            }
        ', [
            'data' => [
                'resetUserPassword' => true,
            ],
        ], [
            'input' => [
                'email' => $user->getEmail(),
            ],
        ]);
    }

    public function testMutationResetUserPasswordWithNonExistingUserEmail()
    {
        $this->assertQueryWithError('
            mutation resetUserPassword($input: ResetUserPasswordInput!) {
                resetUserPassword(input: $input)
            }
        ', [
            'input' => [
                'email' => 'ThisIsAnEmailOfNonExistingUser@exemple.com',
            ],
        ],
            'INVALID_EMAIL'
        );
    }

    public function testMutationSetUserPassword()
    {
        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_RESET_PASSWORD);

        $this->assertQuery('
            mutation setUserPassword($input: SetUserPasswordInput!) {
                setUserPassword(input: $input) {
                    id,
                    email,
                    token
                }
            }
        ', [
            'data' => [
                'setUserPassword' => [
                    'id' => $token->getUser()->getId(),
                    'email' => $token->getUser()->getEmail(),
                    'token' => new GraphQLWildCard('/^[0-9A-Fa-f]+$/'),
                ],
            ],
        ], [
            'input' => [
                'token' => $token->getValue(),
                'password' => 'ThisIsANewPassword',
            ],
        ]);
    }

    public function testMutationSetUserPasswordWithTokenExpired()
    {
        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED_RESET_PASSWORD);

        $this->assertQueryWithError('
            mutation setUserPassword($input: SetUserPasswordInput!) {
                setUserPassword(input: $input) {
                    id,
                    email,
                    token
                }
            }
        ', [
            'input' => [
                'token' => $token->getValue(),
                'password' => 'ThisIsANewPassword',
            ],
        ],
            'TOKEN_EXPIRED'
        );
    }

    public function testMutationSetUserPasswordWithUserNotActivated()
    {
        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_USER_NOT_ACTIVATED_RESET_PASSWORD);

        $this->assertQueryWithError('
            mutation setUserPassword($input: SetUserPasswordInput!) {
                setUserPassword(input: $input) {
                    id,
                    email,
                    token
                }
            }
        ', [
            'input' => [
                'token' => $token->getValue(),
                'password' => 'ThisIsANewPassword',
            ],
        ],
            'USER_NOT_ACCTIVATED'
        );
    }
}
