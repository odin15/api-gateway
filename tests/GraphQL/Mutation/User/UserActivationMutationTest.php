<?php

namespace App\Tests\GraphQL\Mutation\User;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Token;
use App\Gateway\Forward\GraphQlForward;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLTestCase;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLWildCard;

class UserActivationMutationTest extends GraphQLTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    public function testMutationActivateUserAccount()
    {
        $graphQlForward = $this->getMockBuilder(GraphQlForward::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $response = [
            'data' => [
                'incrementUserLicense' => true,
            ],
        ];
        $graphQlForward
            ->expects($this->exactly(1))
            ->method('query')
            ->willReturn($response)
        ;
        $container = $this->getInternalClient()->getContainer();
        $container->set('graphql_forward', $graphQlForward);

        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_USER_NOT_ACTIVATED_ACCOUNT_ACTIVATION);

        $this->assertQuery('
            mutation activateUserAccount($input: ActivateUserAccountInput!) {
                activateUserAccount(input: $input) {
                    id,
                    email,
                    token,
                }
            }
        ', [
            'data' => [
                'activateUserAccount' => [
                    'id' => $token->getUser()->getId(),
                    'email' => $token->getUser()->getEmail(),
                    'token' => new GraphQLWildCard('/^[0-9A-Fa-f]+$/'),
                ],
            ],
        ], [
            'input' => [
                'token' => $token->getValue(),
                'password' => UserFixtures::PASSWORD,
            ],
        ]);
    }

    public function testMutationActivateUserAccountWithInvalidToken()
    {
        $graphQlForward = $this->getMockBuilder(GraphQlForward::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $graphQlForward
            ->expects($this->never())
            ->method('query')
        ;
        $container = $this->getInternalClient()->getContainer();
        $container->set('graphql_forward', $graphQlForward);

        $this->assertQueryWithError('
            mutation activateUserAccount($input: ActivateUserAccountInput!) {
                activateUserAccount(input: $input) {
                    id,
                    email,
                    token,
                }
            }
        ', [
            'input' => [
                'token' => 'ThisIsNotAValidActivationToken',
                'password' => UserFixtures::PASSWORD,
            ],
        ],
            'INVALID_TOKEN'
        );
    }

    public function testMutationActivateUserAccountWithInsufficientLicenses()
    {
        $graphQlForward = $this->getMockBuilder(GraphQlForward::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $response = [
            'data' => [
                'incrementUserLicense' => false,
            ],
        ];
        $graphQlForward
            ->expects($this->exactly(1))
            ->method('query')
            ->willReturn($response)
        ;
        $container = $this->getInternalClient()->getContainer();
        $container->set('graphql_forward', $graphQlForward);

        /** @var Token $token */
        $token = $this->referenceRepository->getReference(TokenFixtures::REFERENCE_TOKEN_USER_NOT_ACTIVATED_ACCOUNT_ACTIVATION);

        $this->assertQueryWithError('
            mutation activateUserAccount($input: ActivateUserAccountInput!) {
                activateUserAccount(input: $input) {
                    id,
                    email,
                    token,
                }
            }
        ', [
            'input' => [
                'token' => $token->getValue(),
                'password' => UserFixtures::PASSWORD,
            ],
        ],
            'INSUFFICIENT_USER_LICENSES'
        );
    }
}
