<?php

namespace App\Tests\GraphQL\Mutation\User;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLTestCase;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLWildCard;

class UserMutationTest extends GraphQLTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            UserFixtures::class,
            TokenFixtures::class,
        ])->getReferenceRepository();
    }

    public function testMutationCreateUser()
    {
        $result = $this->assertQuery('
            mutation createUser($input: CreateUserInput!) {
                createUser(input: $input) {
                    id,
                }
            }
        ', [
            'data' => [
                'createUser' => [
                    'id' => new GraphQLWildCard('/^[0-9]+$/'),
                ],
            ],
        ], [
            'input' => [
                'email' => 'ThisIsAValidEmailAddress@domain.com',
                'roles' => [User::ROLE_ADMIN],
            ],
        ], [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);

        $user = $this->getUserRepository()->find($result['data']['createUser']['id']);
        $this->assertNotNull($user);
    }

    public function testMutationCreateUserWithInvalidToken()
    {
        $this->assertQueryStatus('
            mutation createUser($input: CreateUserInput!) {
                createUser(input: $input) {
                    id,
                }
            }',
            401,
            [
                'input' => [
                    'email' => 'ThisIsAValidEmailAddress@domain.com',
                    'roles' => [User::ROLE_ADMIN],
                ],
            ],
            [
                'Authorization' => 'Bearer InvalidToken',
            ]
        );
    }

    public function testMutationCreateUserWithEmailAlreadyUsed()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQueryWithError('
            mutation createUser($input: CreateUserInput!) {
                createUser(input: $input) {
                    id,
                }
            }
        ', [
            'input' => [
                'email' => $user->getEmail(),
                'roles' => [],
            ],
        ], 'EMAIL_ALREADY_USED', [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);
    }

    public function testMutationCreateUserWithInvalidEmail()
    {
        $this->assertQueryWithError('
            mutation createUser($input: CreateUserInput!) {
                createUser(input: $input) {
                    id,
                }
            }
        ', [
            'input' => [
                'email' => 'This@Is@Not..AValidEmail',
                'roles' => [],
            ],
        ],
        null,
        [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);
    }

    public function testMutationCreateUserWithInvalidRoles()
    {
        $this->assertQueryWithError('
            mutation createUser($input: CreateUserInput!) {
                createUser(input: $input) {
                    id,
                }
            }
        ', [
            'input' => [
                'email' => 'ThisIsAValidEmailAddress@odeven.fr',
                'roles' => ['THIS_ROLE_DOES_NOT_EXIST'],
            ],
        ],
        null,
        [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);
    }

    public function testMutationUpdateUser()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        $newEmail = 'ThisIsANewEmailAddress@odeven.fr';
        $newRoles = [];

        $this->assertQuery('
            mutation updateUser($input: UpdateUserInput!) {
                updateUser(input: $input) {
                    id,
                    email,
                    roles,
                }
            }
        ', [
            'data' => [
                'updateUser' => [
                    'id' => new GraphQLWildCard('/^[0-9]+$/'),
                    'email' => $newEmail,
                    'roles' => $newRoles,
                ],
            ],
        ], [
            'input' => [
                'id' => $user->getId(),
                'email' => $newEmail,
                'roles' => $newRoles,
            ],
        ], [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);
    }

    public function testMutationUpdateUserWithEmailAlreadyUsed()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        /** @var User $user2 */
        $user2 = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_NOT_ACTIVATED);

        $this->assertQueryWithError('
            mutation updateUser($input: UpdateUserInput!) {
                updateUser(input: $input) {
                    id,
                    email,
                }
            }
        ', [
            'input' => [
                'id' => $user->getId(),
                'email' => $user2->getEmail(),
                'roles' => [],
            ],
        ], 'EMAIL_ALREADY_USED', [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);
    }

    public function testMutationUpdateUserWithInvalidEmail()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        $this->assertQueryWithError('
            mutation updateUser($input: UpdateUserInput!) {
                updateUser(input: $input) {
                    id,
                    email,
                }
            }
        ', [
            'input' => [
                'id' => $user->getId(),
                'email' => 'This@Is@Not..AValidEmail',
                'roles' => [User::ROLE_ADMIN],
            ],
        ], null, [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);
    }

    public function testMutationDeleteUser()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $this->assertQuery('
            mutation deleteUser($id: ID!) {
                deleteUser(id: $id)
            }
        ', [
            'data' => [
                'deleteUser' => true,
            ],
        ], [
            'id' => (string) $user->getId(),
        ], [
            'Authorization' => 'Bearer '.TokenFixtures::AUTHENTICATION_TOKEN_ADMIN_VALID,
        ]);

        $user = $this->getUserRepository()->find($user->getId());
        $this->assertNull($user);
    }

    protected function getUserRepository(): UserRepository
    {
        return self::getContainer()->get(UserRepository::class);
    }
}
