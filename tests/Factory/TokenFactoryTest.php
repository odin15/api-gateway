<?php

namespace App\Tests\Factory;

use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Token;
use App\Entity\User;
use App\Factory\TokenFactory;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TokenFactoryTest extends WebTestCase
{
    private ReferenceRepository $referenceRepository;
    private TokenFactory $tokenFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tokenFactory = self::getContainer()->get(TokenFactory::class);

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    /**
     * @dataProvider provideTokenTypes
     */
    public function testCreateToken(string $tokenType)
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);

        $token = $this->tokenFactory->createToken($user, $tokenType);
        $this->assertNotNull($token);
        $this->assertFalse($token->isExpired());
        $this->assertEquals($tokenType, $token->getType());
    }

    public function provideTokenTypes(): array
    {
        return array_map(function ($type) {
            return [$type];
        }, Token::getValidTypes());
    }
}
