<?php

namespace App\Tests\Util;

use App\Util\UrlHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UrlHelperTest extends WebTestCase
{
    public function testGetBaseUrl()
    {
        $container = self::getContainer();
        $urlHelper = $container->get(UrlHelper::class);

        $baseUrl = $urlHelper->getBaseUrl();
        $this->assertEquals('http://{customer}.odin.localhost', $baseUrl);
    }
}
