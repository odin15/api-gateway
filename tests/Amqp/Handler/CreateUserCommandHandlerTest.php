<?php

namespace App\Tests\Amqp\Handler;

use App\Amqp\Handler\CommandHandler\CreateUserCommandHandler;
use App\Amqp\Message\Command\CreateUserCommand;
use App\DataFixtures\TokenFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Gateway\Forward\GraphQlForward;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateUserCommandHandlerTest extends WebTestCase
{
    private ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            TokenFixtures::class,
            UserFixtures::class,
        ])->getReferenceRepository();
    }

    public function testInvokeWithoutPassword()
    {
        $email = 'newUser@example.com';
        $message = (new CreateUserCommand())
            ->setEmail($email)
        ;

        $createUserCommandHandler = self::getContainer()->get(CreateUserCommandHandler::class);
        $createUserCommandHandler($message);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());

        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var User $user */
        $user = $userRepository->findOneByEmail($email);

        $this->assertNotNull($user);
        $this->assertEquals($user->getEmail(), $email);
        $this->assertCount(1, $user->getTokens());
    }

    public function testInvokeWithPassword()
    {
        $email = 'newUser@example.com';
        $message = (new CreateUserCommand())
            ->setEmail($email)
            ->setPassword('test')
        ;

        $graphQlForward = $this->getMockBuilder(GraphQlForward::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $response = [
            'data' => [
                'incrementUserLicense' => true,
            ],
        ];
        $graphQlForward
            ->expects($this->exactly(1))
            ->method('query')
            ->willReturn($response)
        ;
        $container = $this->getContainer();
        $container->set('graphql_forward', $graphQlForward);

        $createUserCommandHandler = self::getContainer()->get(CreateUserCommandHandler::class);
        $createUserCommandHandler($message);

        // If the password is defined, no mail should be sent
        $this->assertCount(0, self::getContainer()->get('messenger.transport.messages')->get());

        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var User $user */
        $user = $userRepository->findOneByEmail($email);

        $this->assertNotNull($user);
        $this->assertNotNull($user->getPassword());
        $this->assertEquals($user->getEmail(), $email);
        $this->assertCount(0, $user->getTokens());
    }

    public function testInvokeWithRoles()
    {
        $email = 'newUser@example.com';
        $roles = [User::ROLE_ADMIN];

        $message = (new CreateUserCommand())
            ->setEmail($email)
            ->setRoles($roles)
        ;

        $createUserCommandHandler = self::getContainer()->get(CreateUserCommandHandler::class);
        $createUserCommandHandler($message);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());

        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var User $user */
        $user = $userRepository->findOneByEmail($email);

        $this->assertNotNull($user);
        $this->assertEquals($user->getEmail(), $email);
        $this->assertEquals($user->getRoles(), $roles);
        $this->assertCount(1, $user->getTokens());
    }

    public function testInvokeWithExistingUser()
    {
        /** @var User $user */
        $user = $this->referenceRepository->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        $message = (new CreateUserCommand())
            ->setEmail($user->getEmail())
        ;

        $createUserCommandHandler = self::getContainer()->get(CreateUserCommandHandler::class);
        $createUserCommandHandler($message);

        $this->assertCount(0, self::getContainer()->get('messenger.transport.messages')->get());

        $userRepository = self::getContainer()->get(UserRepository::class);
        /** @var User[] $users */
        $users = $userRepository->findBy(['email' => $user->getEmail()]);

        $this->assertCount(1, $users);
    }
}
