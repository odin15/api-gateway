<?php

namespace App\Tests\Microservice;

use App\Microservice\ConfigurationLoader;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConfigurationLoaderTest extends WebTestCase
{
    public function testGetEndpoint(): void
    {
        $configurationLoader = self::getContainer()->get(ConfigurationLoader::class);

        $endpoint = $configurationLoader->getEndpoint('bookeo', '1.0', 'graphql');
        $this->assertNotEmpty($endpoint);
    }

    public function testGetEndpointWithNotConfiguredApiType(): void
    {
        $configurationLoader = self::getContainer()->get(ConfigurationLoader::class);

        $this->expectException(\Exception::class);
        $endpoint = $configurationLoader->getEndpoint('customer', '1.0', 'rest');
        $this->assertNotEmpty($endpoint);
    }
}
