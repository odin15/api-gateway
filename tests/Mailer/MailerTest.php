<?php

namespace App\Tests\Mailer;

use App\Mailer\Mailer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Twig\Environment;

class MailerTest extends WebTestCase
{
    public function testSend()
    {
        $subject = 'SUBJECT';
        $text = 'TEXT';
        $html = 'HTML';
        $fromEmail = 'from@email.fr';
        $toEmail = 'to@email.fr';
        $replyToEmail = 'reply-to@email.fr';

        $environment = self::getContainer()->get(Environment::class);
        $template = $environment->createTemplate('
            {% block subject %}'.$subject.'{% endblock %}
            {% block body_text %}'.$text.'{% endblock %}
            {% block body_html %}'.$html.'{% endblock %}
        ');

        $mailer = self::getContainer()->get(Mailer::class);
        $mailer->send($template, [], $toEmail, $fromEmail, $replyToEmail);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());
    }

    public function testSendWithDefaults()
    {
        $subject = 'SUBJECT';
        $text = 'TEXT';
        $html = 'HTML';
        $fromEmail = null;
        $toEmail = 'to@email.fr';
        $replyToEmail = null;

        $environment = self::getContainer()->get(Environment::class);
        $template = $environment->createTemplate('
            {% block subject %}'.$subject.'{% endblock %}
            {% block body_text %}'.$text.'{% endblock %}
            {% block body_html %}'.$html.'{% endblock %}
        ');

        $mailer = self::getContainer()->get(Mailer::class);
        $mailer->send($template, [], $toEmail, $fromEmail, $replyToEmail);

        $this->assertCount(1, self::getContainer()->get('messenger.transport.messages')->get());
    }
}
