<?php

namespace App\Microservice;

use App\Microservice\Model\Configuration;

class ConfigurationLoader
{
    protected array $microservices;

    /** @var Configuration[] */
    protected ?array $configurations = null;

    public function __construct(array $microservices = [])
    {
        $this->microservices = $microservices;
        $this->load();
    }

    public function getServices(): array
    {
        return array_keys($this->microservices);
    }

    public function getEndpoint(string $name, string $version, string $api): string
    {
        $configuration = $this->getConfiguration($name);
        $apis = $configuration->getApis();

        if (!isset($apis[$api])) {
            throw new \Exception('Api of type "'.$api.'" is not defined for service "'.$name.'"');
        }

        $url = $apis[$api]['url'];

        return str_replace('{version}', $version, $url);
    }

    public function getDefaultVersion(string $name): string
    {
        $configuration = $this->getConfiguration($name);

        return $configuration->getDefaultVersion();
    }

    public function getConfiguration(string $name): Configuration
    {
        if (!isset($this->configurations[$name])) {
            throw new \Exception('"'.$name.'" is not a valid service name');
        }

        return $this->configurations[$name];
    }

    protected function load(): void
    {
        if (null === $this->configurations) {
            $this->configurations = [];

            foreach ($this->microservices as $name => $config) {
                $configuration = new Configuration();

                $configuration
                    ->setName($name)
                    ->setApis($config['api'])
                    ->setDefaultVersion($config['version'])
                    ->setPublic($config['public'])
                ;

                $this->configurations[$name] = $configuration;
            }
        }
    }
}
