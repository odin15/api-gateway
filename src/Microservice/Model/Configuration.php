<?php

namespace App\Microservice\Model;

class Configuration
{
    protected string $name;
    protected array $apis;
    protected string $defaultVersion;
    protected bool $public;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getApis(): ?array
    {
        return $this->apis;
    }

    public function setApis(array $apis): self
    {
        $this->apis = $apis;

        return $this;
    }

    public function getDefaultVersion(): ?string
    {
        return $this->defaultVersion;
    }

    public function setDefaultVersion(string $defaultVersion): self
    {
        $this->defaultVersion = $defaultVersion;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }
}
