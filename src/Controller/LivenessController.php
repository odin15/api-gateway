<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class LivenessController
{
    public function alive(): Response
    {
        return new Response('OK');
    }
}
