<?php

namespace App\Controller;

use App\Gateway\Gateway;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GatewayController extends AbstractController
{
    public function __construct(private Gateway $gateway)
    {
    }

    public function redirectToService(Request $request, string $service, string $version, string $rest = ''): Response
    {
        try {
            return $this->gateway->forwardRequest($service, $version, $request, $rest);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }
}
