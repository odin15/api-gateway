<?php

namespace App\Event\Listener;

use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;

class UserListener
{
    public function prePersist(User $user, LifecycleEventArgs $args): void
    {
        $user->setCreatedAt(new \DateTime());
    }
}
