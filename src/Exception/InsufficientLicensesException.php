<?php

namespace App\Exception;

class InsufficientLicensesException extends \Exception
{
    public function __construct()
    {
        parent::__construct('No license available');
    }
}
