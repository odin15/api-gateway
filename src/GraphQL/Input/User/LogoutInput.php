<?php

namespace App\GraphQL\Input\User;

class LogoutInput
{
    private string $token;

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
