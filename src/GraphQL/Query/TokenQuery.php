<?php

namespace App\GraphQL\Query;

use App\Entity\Token;
use App\Repository\TokenRepository;
use Odeven\GraphQLBundle\Bridge\Orm\Query\AbstractQuery;

/**
 * @method TokenRepository getRepository(?string $class = null)
 */
class TokenQuery extends AbstractQuery
{
    public function getClass(): string
    {
        return Token::class;
    }

    public function getToken(string $userId, string $type): ?Token
    {
        return $this->getRepository()->findOneBy([
            'user' => $userId,
            'type' => $type,
        ]);
    }

    public function isTokenValid(string $type, string $value): bool
    {
        $token = $this->getRepository()->findOneByTypeAndValue($type, $value);

        return null !== $token && !$token->isExpired();
    }
}
