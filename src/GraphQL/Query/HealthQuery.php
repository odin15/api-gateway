<?php

namespace App\GraphQL\Query;

use App\Microservice\ConfigurationLoader;
use Jean85\PrettyVersions;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;
use Overblog\GraphQLBundle\Error\UserError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HealthQuery implements QueryInterface
{
    public function __construct(
        private ConfigurationLoader $configurationLoader,
        private HttpClientInterface $client,
        private string $accessToken
    ) {
    }

    public function health(?array $services = []): array
    {
        $result = [
            'version' => (string) PrettyVersions::getVersion('odin/api-gateway'),
        ];

        if (count($services)) {
            $result['services'] = [];
            $availableServices = $this->configurationLoader->getServices();
            foreach ($services as $name => $version) {
                if (!in_array($name, $availableServices, true)) {
                    throw new UserError('"'.$name.'" is not a valid service name');
                }
            }

            foreach ($services as $name => $version) {
                $endpoint = $this->configurationLoader->getEndpoint($name, $version, 'graphql');

                try {
                    $start = microtime(true);
                    $response = $this->client->request(Request::METHOD_OPTIONS, $endpoint, [
                        'headers' => [
                            'X-MicroserviceAccessToken' => $this->accessToken,
                        ],
                    ]);
                    $alive = 200 === $response->getStatusCode();
                    $latency = $alive ? round((microtime(true) - $start) * 1000) : null;
                    $error = !$alive ? $response->getContent() : null;
                } catch (\Exception $e) {
                    $alive = false;
                    $latency = null;
                    $error = $e->getMessage();
                }

                $result['services'][$name] = [
                    'version' => $version,
                    'latency' => $latency,
                    'alive' => $alive,
                    'endpoint' => $endpoint,
                ];

                if ($error) {
                    $result['services'][$name]['error'] = $error;
                }
            }
        }

        return $result;
    }
}
