<?php

namespace App\GraphQL\Query;

use App\Entity\Token;
use App\Entity\User;
use App\Repository\UserRepository;
use Odeven\GraphQLBundle\Bridge\Orm\Query\AbstractQuery;
use Odeven\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Definition\Argument;

/**
 * @method UserRepository getRepository(?string $class = null)
 */
class UserQuery extends AbstractQuery
{
    public function getClass(): string
    {
        return User::class;
    }

    public function findOneByIdOrEmail(Argument $argument): ?User
    {
        if ($argument->offsetExists('id')) {
            return $this->getRepository()->find($argument['id']);
        } elseif ($argument->offsetExists('email')) {
            return $this->getRepository()->findOneByEmail($argument['email']);
        } else {
            throw new UserError('MISSING_IDENTIFIER', 'Missing identifier. You should either set "id" or "email"');
        }
    }

    public function findOneByAuthToken(string $token): ?User
    {
        /** @var Token|null $token */
        $token = $this->getRepository(Token::class)->findOneBy([
            'type' => Token::TYPE_AUTHENTICATION,
            'value' => $token,
        ]);

        if (null === $token) {
            throw new UserError('INVALID_TOKEN');
        }
        if ($token->isExpired()) {
            throw new UserError('TOKEN_EXPIRED');
        }

        return $token->getUser();
    }
}
