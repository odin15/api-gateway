<?php

namespace App\GraphQL\Mutation\User;

use App\Business\CustomerBusiness;
use App\Entity\Token;
use App\Exception\InsufficientLicensesException;
use App\Form\Type\User\ActivateUserAccountType;
use App\GraphQL\Input\User\ActivateUserAccountInput;
use App\GraphQL\Output\UserLoggedInOutput;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use App\Security\UserAuthenticator;
use Odeven\GraphQLBundle\Bridge\Orm\Mutation\AbstractMutation;
use Odeven\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Definition\Argument;

/**
 * @method UserRepository getRepository()
 */
class UserActivationMutation extends AbstractMutation
{
    public function __construct(
        private UserAuthenticator $userAuthenticator,
        private UserAuthenticationMutation $userAuthenticationMutation,
        private CustomerBusiness $customerBusiness
    ) {
    }

    public function activateUserAccount(Argument $input): UserLoggedInOutput
    {
        $model = new ActivateUserAccountInput();
        $form = $this->createForm(ActivateUserAccountType::class, $model);
        $this->submit($form, $input, true);

        /** @var TokenRepository $tokenRepository */
        $tokenRepository = $this->em->getRepository(Token::class);
        $token = $tokenRepository->findOneByTypeAndValue(Token::TYPE_ACCOUNT_ACTIVATION, $model->getToken());
        if (null === $token) {
            throw new UserError('INVALID_TOKEN');
        }

        try {
            $this->customerBusiness->incrementUserLicense();
        } catch (InsufficientLicensesException $e) {
            throw new UserError('INSUFFICIENT_USER_LICENSES');
        }

        $user = $token->getUser();
        $user
            ->setActivated(true)
            ->setPassword($this->userAuthenticator->encodePassword($user, $model->getPassword()))
        ;
        $this->em->remove($token);
        $this->em->flush();

        return $this->userAuthenticationMutation->login(new Argument([
            'email' => $user->getEmail(),
            'password' => $input['password'],
        ]));
    }
}
