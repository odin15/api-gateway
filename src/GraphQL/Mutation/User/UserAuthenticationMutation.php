<?php

namespace App\GraphQL\Mutation\User;

use App\Amqp\Message\Event\UserLoggedInEvent;
use App\Amqp\Message\Event\UserLoggedOutEvent;
use App\Entity\Token;
use App\Form\Type\User\LoginType;
use App\Form\Type\User\LogoutType;
use App\GraphQL\Input\User\LoginInput;
use App\GraphQL\Input\User\LogoutInput;
use App\GraphQL\Output\UserLoggedInOutput;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use App\Security\UserAuthenticator;
use Odeven\GraphQLBundle\Bridge\Orm\Mutation\AbstractMutation;
use Odeven\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * @method UserRepository getRepository()
 */
class UserAuthenticationMutation extends AbstractMutation
{
    private UserAuthenticator $userAuthenticator;
    private MessageBusInterface $bus;

    /** @required */
    public function setUserAuthenticator(UserAuthenticator $userAuthenticator)
    {
        $this->userAuthenticator = $userAuthenticator;
    }

    /** @required */
    public function setMessageBusInterface(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function login(Argument $input): UserLoggedInOutput
    {
        $model = new LoginInput();
        $form = $this->createForm(LoginType::class, $model);
        $this->submit($form, $input, true);

        try {
            $token = $this->userAuthenticator->authenticate($model->getEmail(), $model->getPassword());
            $user = $token->getUser();
            $userLoggedInOutput = new UserLoggedInOutput();
            $userLoggedInOutput
                ->setId($user->getId())
                ->setEmail($user->getEmail())
                ->setRoles($user->getRoles())
                ->setToken($token->getValue())
            ;

            $userLoggedInEvent = new UserLoggedInEvent();
            $userLoggedInEvent
                ->setId($user->getId())
                ->setDateTime(new \DateTime())
            ;

            $this->bus->dispatch($userLoggedInEvent);

            return $userLoggedInOutput;
        } catch (BadCredentialsException $e) {
            throw new UserError('BAD_CREDENTIALS', 'Bad credentials');
        }
    }

    public function logout(Argument $input): bool
    {
        $model = new LogoutInput();
        $form = $this->createForm(LogoutType::class, $model);
        $this->submit($form, $input, true);

        /** @var TokenRepository $tokenRepository */
        $tokenRepository = $this->em->getRepository(Token::class);
        $token = $tokenRepository->findOneBy([
            'type' => Token::TYPE_AUTHENTICATION,
            'value' => $model->getToken(),
        ]);

        if (null !== $token) {
            $this->em->remove($token);
            $this->em->flush();

            $userLoggedOutEvent = new UserLoggedOutEvent();
            $userLoggedOutEvent
                ->setId($token->getUser()->getId())
                ->setDateTime(new \DateTime())
            ;

            $this->bus->dispatch($userLoggedOutEvent);

            return true;
        }

        return false;
    }
}
