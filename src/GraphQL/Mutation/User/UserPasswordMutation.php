<?php

namespace App\GraphQL\Mutation\User;

use App\Entity\Token;
use App\Entity\User;
use App\Factory\TokenFactory;
use App\GraphQL\Output\UserLoggedInOutput;
use App\Mailer\ResetUserPasswordMailer;
use App\Mailer\UserPasswordChangedMailer;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use App\Security\UserAuthenticator;
use Odeven\GraphQLBundle\Bridge\Orm\Mutation\AbstractMutation;
use Odeven\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Definition\Argument;

class UserPasswordMutation extends AbstractMutation
{
    private UserAuthenticator $userAuthenticator;
    private TokenFactory $tokenFactory;
    private ResetUserPasswordMailer $resetUserPasswordMailer;
    private UserPasswordChangedMailer $userPasswordChangedMailer;
    private UserAuthenticationMutation $userAuthenticationMutation;

    /** @required */
    public function setUserAuthenticator(UserAuthenticator $userAuthenticator)
    {
        $this->userAuthenticator = $userAuthenticator;
    }

    /** @required */
    public function setTokenFactory(TokenFactory $tokenFactory)
    {
        $this->tokenFactory = $tokenFactory;
    }

    /** @required */
    public function setResetUserPasswordMailer(ResetUserPasswordMailer $resetUserPasswordMailer)
    {
        $this->resetUserPasswordMailer = $resetUserPasswordMailer;
    }

    /** @required */
    public function setUserPasswordChangedMailer(UserPasswordChangedMailer $userPasswordChangedMailer)
    {
        $this->userPasswordChangedMailer = $userPasswordChangedMailer;
    }

    /** @required */
    public function setUserAuthenticationMutation(UserAuthenticationMutation $userAuthenticationMutation)
    {
        $this->userAuthenticationMutation = $userAuthenticationMutation;
    }

    public function resetUserPassword(Argument $input): bool
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository(User::class);
        $user = $userRepository->findOneByEmail($input['email']);
        if (null === $user) {
            throw new UserError('INVALID_EMAIL', 'Email does not exist');
        }

        /** @var TokenRepository $tokenRepository */
        $tokenRepository = $this->em->getRepository(Token::class);
        $token = $tokenRepository->findOneByUserAndType($user, Token::TYPE_RESET_PASSWORD);
        if (null === $token) {
            $token = $this->tokenFactory->createToken($user, Token::TYPE_RESET_PASSWORD);
            $this->em->persist($token);
        } else {
            $this->tokenFactory->resetToken($token);
        }
        $this->em->flush();

        $this->resetUserPasswordMailer->send($user, $token);

        return true;
    }

    public function setUserPassword(Argument $input): UserLoggedInOutput
    {
        /** @var TokenRepository $tokenRepository */
        $tokenRepository = $this->em->getRepository(Token::class);
        $token = $tokenRepository->findOneByTypeAndValue(Token::TYPE_RESET_PASSWORD, $input['token']);
        if (null === $token || $token->isExpired()) {
            throw new UserError('TOKEN_EXPIRED', 'Token has expired');
        }

        $user = $token->getUser();
        if (!$user->isActivated()) {
            throw new UserError('USER_NOT_ACCTIVATED', 'Your must activate your account first');
        }

        $user->setPassword($this->userAuthenticator->encodePassword($user, $input['password']));
        $this->em->remove($token);
        $this->em->flush();

        $this->userPasswordChangedMailer->send($user);

        return $this->userAuthenticationMutation->login(new Argument([
            'email' => $user->getEmail(),
            'password' => $input['password'],
        ]));
    }
}
