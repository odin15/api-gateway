<?php

namespace App\GraphQL\Mutation\User;

use App\Entity\User;
use App\Factory\UserFactory;
use App\Form\Type\User\CreateUserType;
use App\Form\Type\User\UpdateUserType;
use App\Repository\UserRepository;
use Odeven\GraphQLBundle\Bridge\Orm\Mutation\AbstractMutation;
use Odeven\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Definition\Argument;

class UserMutation extends AbstractMutation
{
    private UserFactory $userFactory;

    /** @required */
    public function setUserFactory(UserFactory $userFactory)
    {
        $this->userFactory = $userFactory;
    }

    public function createUser(Argument $input): User
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository(User::class);

        if (null !== $userRepository->findOneByEmail($input['email'])) {
            throw new UserError('EMAIL_ALREADY_USED');
        }

        $user = new User();
        $form = $this->createForm(CreateUserType::class, $user);
        $this->submit($form, $input, true);

        return $this->userFactory->createUser(
            $user->getEmail(),
            $user->getRoles(),
            $user->getPassword(),
        );
    }

    public function updateUser(Argument $input): User
    {
        /** @var User $user */
        $user = $this->getEntity(User::class, $input['id'], 'USER_NOT_FOUND');

        if ($user->getEmail() !== $input['email']) {
            /** @var UserRepository $userRepository */
            $userRepository = $this->em->getRepository(User::class);

            if (null !== $userRepository->findOneByEmail($input['email'])) {
                throw new UserError('EMAIL_ALREADY_USED');
            }
        }

        $form = $this->createForm(UpdateUserType::class, $user);
        unset($input['id']);

        $this->submit($form, $input, true);
        $this->em->flush();

        return $user;
    }

    public function deleteUser(Argument $input): bool
    {
        $user = $this->getEntity(User::class, $input['id'], 'USER_NOT_FOUND');

        $this->em->remove($user);
        $this->em->flush();

        return true;
    }
}
