<?php

namespace App\Mailer;

trait MailerAwareTrait
{
    protected Mailer $mailer;

    /** @required */
    public function setMailer(Mailer $mailer): void
    {
        $this->mailer = $mailer;
    }
}
