<?php

namespace App\Mailer;

use App\Entity\Token;
use App\Entity\User;
use App\Util\UrlHelper;

class RegistrationMailer
{
    use MailerAwareTrait;

    public function __construct(private UrlHelper $urlHelper)
    {
    }

    public function send(User $user, Token $token)
    {
        $activationUrl = $this->urlHelper->getBaseUrl().'/auth/user/activation/'.$token->getValue();

        $this->mailer->send(
            'mail/registration.html.twig',
            [
                'actionLink' => $activationUrl,
                'actionText' => 'Activer mon compte',
            ],
            $user->getEmail()
        );
    }
}
