<?php

namespace App\Mailer;

use App\Entity\User;

class UserPasswordChangedMailer
{
    use MailerAwareTrait;

    public function send(User $user)
    {
        $this->mailer->send(
            'mail/user_password_changed.html.twig',
            [],
            $user->getEmail()
        );
    }
}
