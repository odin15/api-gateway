<?php

namespace App\Mailer;

use Odevia\MailerBundle\Amqp\Message\Command\SendEmailCommand;
use Symfony\Component\Messenger\MessageBusInterface;
use Twig\Environment;
use Twig\TemplateWrapper;

class Mailer
{
    public function __construct(private MessageBusInterface $bus, private Environment $twig)
    {
    }

    /**
     * @throws \Throwable
     */
    public function send(
        string|TemplateWrapper $template,
        array $context,
        array|string $toEmail,
        array|string|null $fromEmail = null,
        ?string $replyTo = null
    ) {
        try {
            $context = $this->twig->mergeGlobals($context);
            $template = $this->twig->load($template);
            $subject = $template->renderBlock('subject', $context);
            $textBody = $template->renderBlock('body_text', $context);
            $htmlBody = $template->renderBlock('body_html', $context);

            $email = (new SendEmailCommand())
                ->setSubject($subject)
                ->setFrom($fromEmail)
                ->setTo($toEmail)
            ;

            if (null !== $replyTo) {
                $email->setReplyTo($replyTo);
            }
            if (!empty($htmlBody)) {
                $email->setHtml($htmlBody);
            }
            if (!empty($textBody)) {
                $email->setText($textBody);
            }

            $this->bus->dispatch($email);
        } catch (\Exception $e) {
            throw new \Exception('An error occured while sending a mail : '.$e->getMessage(), 0, $e);
        }
    }
}
