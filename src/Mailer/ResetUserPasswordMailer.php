<?php

namespace App\Mailer;

use App\Entity\Token;
use App\Entity\User;
use App\Util\UrlHelper;

class ResetUserPasswordMailer
{
    use MailerAwareTrait;

    public function __construct(private UrlHelper $urlHelper)
    {
    }

    public function send(User $user, Token $token)
    {
        $resetUrl = $this->urlHelper->getBaseUrl().'/auth/password/reset/'.$token->getValue();

        $this->mailer->send(
            'mail/reset_user_password.html.twig',
            [
                'actionLink' => $resetUrl,
                'actionText' => 'Modifier mon mot de passe',
            ],
            $user->getEmail()
        );
    }
}
