<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class MicroserviceTokenAuthenticator extends AbstractAuthenticator
{
    public function __construct(private UserRepository $userRepository, private string $accessToken)
    {
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('X-MicroserviceAccessToken') &&
            $request->headers->has('X-User')
        ;
    }

    public function authenticate(Request $request): Passport
    {
        $accessToken = $request->headers->get('X-MicroserviceAccessToken');
        if (null === $accessToken) {
            throw new CustomUserMessageAuthenticationException('No microservice access token provided');
        }

        if ($this->accessToken !== $accessToken) {
            throw new CustomUserMessageAuthenticationException('Invalid microservice access token');
        }

        $userId = $request->headers->get('X-User');
        if (null === $userId) {
            throw new CustomUserMessageAuthenticationException('No user provided');
        }

        return new SelfValidatingPassport(new UserBadge((string) $userId, [$this, 'getUser']));
    }

    public function getUser(string $id): ?User
    {
        return $this->userRepository->find((int) $id);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}
