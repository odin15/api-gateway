<?php

namespace App\Security;

use App\Entity\Token;
use App\Entity\User;
use App\Factory\TokenFactory;
use App\Repository\TokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class UserTokenAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private TokenRepository $tokenRepository,
        private TokenFactory $tokenFactory,
        private EntityManagerInterface $em
    ) {
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('Authorization');
    }

    public function authenticate(Request $request): Passport
    {
        $token = $request->headers->get('Authorization');
        if (null === $token) {
            throw new CustomUserMessageAuthenticationException('No user token provided');
        }

        if ('bearer ' !== strtolower(substr($token, 0, 7))) {
            throw new CustomUserMessageAuthenticationException('Token should be formated as "Bearer $token"');
        }

        $token = substr($token, 7);

        return new SelfValidatingPassport(new UserBadge($token, [$this, 'getUser']));
    }

    public function getUser(string $tokenValue): ?User
    {
        $token = $this->tokenRepository->findOneByTypeAndValue(Token::TYPE_AUTHENTICATION, $tokenValue);

        if (null === $token) {
            return null;
        } else {
            $this->tokenFactory->resetExpiresAt($token);
            $this->em->flush();
        }

        return $token->getUser();
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse([
            'errors' => [
                $exception->getMessage(),
            ],
        ], Response::HTTP_UNAUTHORIZED);
    }
}
