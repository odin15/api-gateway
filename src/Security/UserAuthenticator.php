<?php

namespace App\Security;

use App\Entity\Token;
use App\Entity\User;
use App\Factory\TokenFactory;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class UserAuthenticator
{
    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher,
        private UserRepository $userRepository,
        private TokenRepository $tokenRepository,
        private TokenFactory $tokenFactory,
        private EntityManagerInterface $em
    ) {
    }

    public function authenticate(string $email, string $password): Token
    {
        $user = $this->userRepository->findOneByEmail($email);

        if (null === $user || !$this->userPasswordHasher->isPasswordValid($user, $password)) {
            throw new BadCredentialsException();
        }

        $token = $this->tokenRepository->findOneBy([
            'user' => $user,
            'type' => Token::TYPE_AUTHENTICATION,
        ]);
        if (null === $token) {
            $token = $this->tokenFactory->createToken($user, Token::TYPE_AUTHENTICATION);
            $this->em->persist($token);
        } else {
            $this->tokenFactory->resetToken($token);
        }

        $this->em->flush();

        return $token;
    }

    public function encodePassword(User $user, string $plainPassword): string
    {
        return $this->userPasswordHasher->hashPassword($user, $plainPassword);
    }
}
