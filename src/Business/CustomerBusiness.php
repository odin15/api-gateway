<?php

namespace App\Business;

use App\Exception\InsufficientLicensesException;
use App\Gateway\Forward\GraphQlForward;

class CustomerBusiness
{
    public function __construct(private GraphQlForward $graphQlForward, private string $accessToken)
    {
    }

    public function incrementUserLicense()
    {
        $query = '
            mutation incrementUserLicense {
              incrementUserLicense
            }
        ';

        $headers = [
            'X-MicroserviceAccessToken' => $this->accessToken,
            'X-Security-Check' => true,
            'X-Service' => 'api-gateway',
        ];

        $result = $this->graphQlForward->query('customer', $query, [], null, $headers);
        if (!isset($result['data']['incrementUserLicense']) || !$result['data']['incrementUserLicense']) {
            throw new InsufficientLicensesException();
        }
    }
}
