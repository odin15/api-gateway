<?php

namespace App\Command\User;

use App\Amqp\Message\Command\CreateUserCommand;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateCommand extends Command
{
    public function __construct(
        private EntityManagerInterface $em,
        private MessageBusInterface $bus
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:user:create')
            ->setDescription('Use this command for dev purpose only : start all workers')
            ->addArgument('email', InputArgument::REQUIRED, 'email address of the user to create')
            ->addOption('password', 'p', InputOption::VALUE_OPTIONAL, 'password of the user to create')
            ->addOption('roles', 'r', InputOption::VALUE_OPTIONAL, 'roles of the user to create (comma separated)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = $input->getArgument('email');
        $password = $input->getOption('password');
        $roles = $input->getOption('roles');
        if (null !== $roles) {
            $roles = explode(',', $roles);
            foreach ($roles as $role) {
                if (!in_array($role, User::getValidRoles(), true)) {
                    $output->writeln('ERROR: "'.$role.'" is not a valid role');

                    return 1;
                }
            }
        } else {
            $roles = [];
        }

        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository(User::class);
        if (null !== $userRepository->findOneByEmail($email)) {
            $output->writeln('User with email "'.$email.'" already exists. Skipping...');

            return 0;
        }

        $output->writeln('Creating new user "'.$email.'"');

        $createUserCommand = (new CreateUserCommand())
            ->setEmail($email)
            ->setRoles($roles)
        ;

        if (null !== $password) {
            $createUserCommand->setPassword($password);
        }

        $this->bus->dispatch($createUserCommand);

        $output->writeln('SUCCESS: User will be created');
        if (null === $password) {
            $output->writeln('INFO: you should receive an email to confirm your account');
        }

        return 0;
    }
}
