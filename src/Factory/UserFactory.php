<?php

namespace App\Factory;

use App\Business\CustomerBusiness;
use App\Entity\Token;
use App\Entity\User;
use App\Mailer\RegistrationMailer;
use App\Security\UserAuthenticator;
use Doctrine\ORM\EntityManagerInterface;

class UserFactory
{
    public function __construct(
        private UserAuthenticator $userAuthenticator,
        private RegistrationMailer $registrationMailer,
        private TokenFactory $tokenFactory,
        private EntityManagerInterface $em,
        private CustomerBusiness $customerBusiness
    ) {
    }

    public function createUser(string $email, array $roles = [], ?string $password = null, bool $flush = true): User
    {
        $user = (new User())
            ->setEmail($email)
            ->setRoles($roles)
        ;
        $this->em->persist($user);

        if (null !== $password) {
            $this->customerBusiness->incrementUserLicense();

            $user
                ->setPassword($this->userAuthenticator->encodePassword($user, $password))
                ->setActivated(true)
            ;

            if ($flush) {
                $this->em->flush();
            }
        } else {
            $token = $this->tokenFactory->createToken($user, Token::TYPE_ACCOUNT_ACTIVATION);

            if ($flush) {
                $this->em->flush();
            }

            $this->registrationMailer->send($user, $token);
        }

        return $user;
    }
}
