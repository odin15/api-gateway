<?php

namespace App\Factory;

use App\Entity\Token;
use App\Entity\User;

class TokenFactory
{
    private const DATE_INTERVAL_EXPIRATION_AUTHENTICATION = 'P1D';
    private const DATE_INTERVAL_EXPIRATION_RESET_PASSWORD = 'P1D';

    public function createToken(User $user, string $type): Token
    {
        $token = (new Token())
            ->setType($type)
        ;

        $user->addToken($token);

        $this->resetToken($token);

        return $token;
    }

    public function resetToken(Token $token): self
    {
        $this
            ->resetValue($token)
            ->resetExpiresAt($token)
        ;

        return $this;
    }

    public function resetValue(Token $token): self
    {
        $token->setValue(bin2hex(openssl_random_pseudo_bytes(16)));

        return $this;
    }

    public function resetExpiresAt(Token $token): self
    {
        switch ($token->getType()) {
            case Token::TYPE_ACCOUNT_ACTIVATION:
                break;
            case Token::TYPE_AUTHENTICATION:
                $token
                    ->setExpiresAt((new \DateTime())
                        ->add(new \DateInterval(self::DATE_INTERVAL_EXPIRATION_AUTHENTICATION))
                    )
                ;
                break;
            case Token::TYPE_RESET_PASSWORD:
                $token
                    ->setExpiresAt((new \DateTime())
                        ->add(new \DateInterval(self::DATE_INTERVAL_EXPIRATION_RESET_PASSWORD))
                    )
                ;
                break;
        }

        return $this;
    }
}
