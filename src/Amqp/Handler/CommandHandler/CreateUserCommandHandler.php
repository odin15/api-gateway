<?php

namespace App\Amqp\Handler\CommandHandler;

use App\Amqp\Message\Command\CreateUserCommand;
use App\Entity\User;
use App\Factory\UserFactory;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateUserCommandHandler implements MessageHandlerInterface
{
    public function __construct(private UserFactory $userFactory, private EntityManagerInterface $em)
    {
    }

    public function __invoke(CreateUserCommand $message)
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository(User::class);
        $user = $userRepository->findOneByEmail($message->getEmail());
        if (null === $user) {
            $this->userFactory->createUser($message->getEmail(), $message->getRoles(), $message->getPassword());
        }
    }
}
