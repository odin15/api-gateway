<?php

namespace App\Amqp\Message\Event;

use Odeven\AmqpBundle\Message\MessageInterface;

class UserLoggedOutEvent implements MessageInterface
{
    private int $id;
    private \DateTimeInterface $dateTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }
}
