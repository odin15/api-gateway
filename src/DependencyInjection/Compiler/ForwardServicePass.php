<?php

namespace App\DependencyInjection\Compiler;

use App\Gateway\Forward\ForwardStrategy;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ForwardServicePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        /** @var bool $exists */
        $exists = $container->has(ForwardStrategy::class);
        if (!$exists) {
            return;
        }

        $definition = $container->findDefinition(ForwardStrategy::class);

        $taggedServices = $container->findTaggedServiceIds('app.gateway.forward');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addForwardService', [new Reference($id)]);
        }
    }
}
