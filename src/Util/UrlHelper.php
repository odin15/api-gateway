<?php

namespace App\Util;

class UrlHelper
{
    public function __construct(private string $webAppEndpoint)
    {
    }

    public function getBaseUrl(): string
    {
        return $this->webAppEndpoint;
    }
}
