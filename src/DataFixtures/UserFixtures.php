<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public const REFERENCE_USER_ADMIN = 'REFERENCE_USER_ADMIN';
    public const REFERENCE_USER_NOT_ACTIVATED = 'REFERENCE_USER_NOT_ACTIVATED';
    public const REFERENCE_USER_WITH_PASSWORD_FORGOTTEN = 'REFERENCE_USER_WITH_PASSWORD_FORGOTTEN';
    public const REFERENCE_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED = 'REFERENCE_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED';

    public const PASSWORD = 'azerty';

    public function __construct(private UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    public function load(ObjectManager $manager)
    {
        // Admin user (activated)
        $userAdmin = (new User())
            ->setUsername('admin')
            ->setEmail('admin@example.com')
            ->setActivated(true)
            ->setCreatedAt(new \DateTime('2020-12-31 23:59:59'))
        ;
        $userAdmin->setPassword($this->userPasswordHasher->hashPassword($userAdmin, self::PASSWORD));
        $this->addReference(self::REFERENCE_USER_ADMIN, $userAdmin);
        $manager->persist($userAdmin);

        // Lambda user (not activated)
        $userNotActivated = (new User())
            ->setUsername('Jean Marius')
            ->setEmail('not-activated@example.com')
            ->setActivated(false)
            ->setCreatedAt(new \DateTime('2020-12-31 23:59:59'))
        ;
        $this->addReference(self::REFERENCE_USER_NOT_ACTIVATED, $userNotActivated);
        $manager->persist($userNotActivated);

        // Lambda user (activated, password forgotten)
        $userWithPasswordForgotten = (new User())
            ->setUsername('Bertrand Massieux')
            ->setEmail('password-forgotten@example.com')
            ->setActivated(true)
            ->setCreatedAt(new \DateTime('2020-12-31 23:59:59'))
        ;
        $userWithPasswordForgotten->setPassword($this->userPasswordHasher->hashPassword($userWithPasswordForgotten, self::PASSWORD));
        $this->addReference(self::REFERENCE_USER_WITH_PASSWORD_FORGOTTEN, $userWithPasswordForgotten);
        $manager->persist($userWithPasswordForgotten);

        // Lambda user (activated, password forgotten expired)
        $userWithPasswordForgottenExpired = (new User())
            ->setUsername('Vincent Marius')
            ->setEmail('password-forgotten-expired@example.com')
            ->setActivated(true)
            ->setCreatedAt(new \DateTime('2020-12-31 23:59:59'))
        ;
        $userWithPasswordForgottenExpired->setPassword($this->userPasswordHasher->hashPassword($userWithPasswordForgottenExpired, self::PASSWORD));
        $this->addReference(self::REFERENCE_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED, $userWithPasswordForgottenExpired);
        $manager->persist($userWithPasswordForgottenExpired);

        $manager->flush();
    }
}
