<?php

namespace App\DataFixtures;

use App\Entity\Token;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TokenFixtures extends Fixture implements DependentFixtureInterface
{
    public const AUTHENTICATION_TOKEN_ADMIN_VALID = 'ThisTokenIsAValidAuthenticationToken';

    public const REFERENCE_TOKEN_ADMIN_AUTHENTICATION = 'REFERENCE_TOKEN_ADMIN_AUTHENTICATION';
    public const REFERENCE_TOKEN_USER_NOT_ACTIVATED_ACCOUNT_ACTIVATION = 'REFERENCE_TOKEN_USER_NOT_ACTIVATED_ACCOUNT_ACTIVATION';
    public const REFERENCE_TOKEN_USER_NOT_ACTIVATED_RESET_PASSWORD = 'REFERENCE_TOKEN_USER_NOT_ACTIVATED_RESET_PASSWORD';
    public const REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_RESET_PASSWORD = 'REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_RESET_PASSWORD';
    public const REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED_RESET_PASSWORD = 'REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED_RESET_PASSWORD';
    public const REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_AUTHENTICATION_TOKEN_EXPIRED = 'REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_AUTHENTICATION_TOKEN_EXPIRED';

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        /** @var User $userAdmin */
        $userAdmin = $this->getReference(UserFixtures::REFERENCE_USER_ADMIN);
        /** @var User $userNotActivated */
        $userNotActivated = $this->getReference(UserFixtures::REFERENCE_USER_NOT_ACTIVATED);
        /** @var User $userWithPasswordForgotten */
        $userWithPasswordForgotten = $this->getReference(UserFixtures::REFERENCE_USER_WITH_PASSWORD_FORGOTTEN);
        /** @var User $userWithPasswordForgottenExpired */
        $userWithPasswordForgottenExpired = $this->getReference(UserFixtures::REFERENCE_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED);

        // Authentication Token for user "admin@example.com"
        $token = (new Token())
            ->setType(Token::TYPE_AUTHENTICATION)
            ->setValue(self::AUTHENTICATION_TOKEN_ADMIN_VALID)
            ->setExpiresAt((new \DateTime())->add(new \DateInterval('P1D')))
            ->setUser($userAdmin)
        ;
        $this->setReference(self::REFERENCE_TOKEN_ADMIN_AUTHENTICATION, $token);
        $manager->persist($token);

        // Account Activation Token for user "not-activated@example.com"
        $token = (new Token())
            ->setType(Token::TYPE_ACCOUNT_ACTIVATION)
            ->setValue('ThisTokenIsAValidActivationToken')
            ->setUser($userNotActivated)
        ;
        $this->setReference(self::REFERENCE_TOKEN_USER_NOT_ACTIVATED_ACCOUNT_ACTIVATION, $token);
        $manager->persist($token);

        // Reset Password Token for user "not-activated@example.com"
        $token = (new Token())
            ->setType(Token::TYPE_RESET_PASSWORD)
            ->setValue('ResetPasswordTokenForUserNotActivated')
            ->setExpiresAt((new \DateTime())->add(new \DateInterval('P1D')))
            ->setUser($userNotActivated)
        ;
        $this->setReference(self::REFERENCE_TOKEN_USER_NOT_ACTIVATED_RESET_PASSWORD, $token);
        $manager->persist($token);

        // Reset Password Token for user "password-forgotten@example.com"
        $token = (new Token())
            ->setType(Token::TYPE_RESET_PASSWORD)
            ->setValue('ThisTokenIsAValidResetPasswordToken')
            ->setExpiresAt((new \DateTime())->add(new \DateInterval('P1D')))
            ->setUser($userWithPasswordForgotten)
        ;
        $this->setReference(self::REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_RESET_PASSWORD, $token);
        $manager->persist($token);

        // Authentication Token (expired) for user "password-forgotten@example.com"
        $token = (new Token())
            ->setType(Token::TYPE_AUTHENTICATION)
            ->setValue('ThisTokenIsAnInvalidAuthenticationToken')
            ->setExpiresAt((new \DateTime())->sub(new \DateInterval('P1D')))
            ->setUser($userWithPasswordForgotten)
        ;
        $this->setReference(self::REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_AUTHENTICATION_TOKEN_EXPIRED, $token);
        $manager->persist($token);

        // Reset Password Token (expired) for user "password-forgotten-expired@example.com"
        $token = (new Token())
            ->setType(Token::TYPE_RESET_PASSWORD)
            ->setValue('ThisTokenIsAnInvalidResetPasswordToken')
            ->setExpiresAt((new \DateTime())->sub(new \DateInterval('P1D')))
            ->setUser($userWithPasswordForgottenExpired)
        ;
        $this->setReference(self::REFERENCE_TOKEN_USER_WITH_PASSWORD_FORGOTTEN_EXPIRED_RESET_PASSWORD, $token);
        $manager->persist($token);

        $manager->flush();
    }
}
