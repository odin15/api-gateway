<?php

namespace App\Gateway\Forward;

use App\Entity\User;
use App\Microservice\ConfigurationLoader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;

class ForwardStrategy
{
    /** @var ForwardInterface[] */
    private array $forwardServices = [];

    public function __construct(
        private ConfigurationLoader $configurationLoader,
        private Security $security,
        private UserPasswordHasherInterface $userPasswordHasher,
        private string $accessToken
    ) {
    }

    public function forward(string $serviceName, string $version, Request $request, string $rest): Response
    {
        $type = !empty($rest)
            ? RestForward::TYPE
            : GraphQlForward::TYPE;

        $forwardService = $this->getForwardService($type);

        try {
            if (!$this->configurationLoader->getConfiguration($serviceName)->isPublic() &&
                null === $this->security->getUser() &&
                !$this->isServiceAuthenticated($request)
            ) {
                return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
            }

            $headers = $this->getHeaders($type, $request);

            return $forwardService->forward($serviceName, $version, $rest, $request, $headers);
        } catch (\Exception $e) {
            return $forwardService->generateErrorResponse($e);
        }
    }

    public function addForwardService(ForwardInterface $forwardService): void
    {
        $this->forwardServices[$forwardService->getType()] = $forwardService;
    }

    public function getForwardService(string $type): ForwardInterface
    {
        if (!isset($this->forwardServices[$type])) {
            throw new \Exception('No forward service found for type "'.$type.'"');
        }

        return $this->forwardServices[$type];
    }

    private function getHeaders(string $type, Request $request): array
    {
        $headers = [
            'X-MicroserviceAccessToken' => $this->accessToken,
        ];

        /** @var User $user */
        $user = $this->security->getUser();
        if (null !== $user) {
            $headers['X-User'] = $user->getId();
            $headers['X-Roles'] = implode(',', $user->getRoles());
        }

        $securityCheck = $request->headers->get('X-Security-Check');
        if (null !== $securityCheck) {
            if (!$this->userPasswordHasher->isPasswordValid($user, $securityCheck)) {
                throw $this->getForwardService($type)->getException('INVALID_PASSWORD');
            } else {
                $headers['X-Security-Check'] = true;
            }
        }

        $accessToken = $request->headers->get('X-MicroserviceAccessToken');
        if (null !== $accessToken) {
            if ($this->accessToken !== $accessToken) {
                throw $this->getForwardService($type)->getException('ACCESS_UNAUTHORIZED', 'Microservice access token is invalid');
            }

            if ($request->headers->has('X-Service')) {
                $headers['X-Service'] = $request->headers->get('X-Service');
            } else {
                throw $this->getForwardService($type)->getException('MISSING_HEADERS', 'You have to set X-Service in your headers');
            }
        }

        return $headers;
    }

    public function isServiceAuthenticated(Request $request): bool
    {
        return $request->headers->has('X-Service') && $this->accessToken === $request->headers->get('X-MicroserviceAccessToken');
    }
}
