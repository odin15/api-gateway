<?php

namespace App\Gateway\Forward;

use App\Microservice\ConfigurationLoader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RestForward implements ForwardInterface
{
    public const TYPE = 'rest';

    public function __construct(
        private ConfigurationLoader $configurationLoader,
        private HttpClientInterface $client
    ) {
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function forward(string $serviceName, ?string $version, string $rest, Request $request, array $headers = []): Response
    {
        $options = [
            'headers' => $headers,
        ];

        if (Request::METHOD_GET !== $request->getMethod()) {
            $options['body'] = $request->getContent();
        }

        if (null === $version) {
            $version = $this->configurationLoader->getDefaultVersion($serviceName);
        }

        $response = $this->client->request(
            $request->getMethod(),
            $this->getEndpoint($serviceName, $version, $rest),
            $options
        );

        return new Response(
            $response->getContent(false),
            $response->getStatusCode(),
            $response->getHeaders(false)
        );
    }

    private function getEndpoint(string $serviceName, string $version, string $rest)
    {
        $endpoint = $this->configurationLoader->getEndpoint($serviceName, $version, self::TYPE);

        if ('/' !== substr($endpoint, -1)) {
            $endpoint .= '/';
        }

        return $endpoint.$rest;
    }

    public function getException(string $code, string $message = ''): \Exception
    {
        return new \Exception($code);
    }

    public function generateErrorResponse(\Exception $exception): Response
    {
        return new JsonResponse([
            'errors' => [$exception->getCode()],
        ], 500);
    }
}
