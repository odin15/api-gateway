<?php

namespace App\Gateway\Forward;

use App\Gateway\Client;
use App\Gateway\ClientBuilder;
use App\Microservice\ConfigurationLoader;
use GraphQL\Error\DebugFlag;
use GraphQL\Error\FormattedError;
use Odeven\GraphQLBundle\Error\UserError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GraphQlForward implements ForwardInterface
{
    public const TYPE = 'graphql';

    public function __construct(private ConfigurationLoader $configurationLoader, private ClientBuilder $clientBuilder)
    {
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function query(string $serviceName, string $query, array $variables = [], ?string $version = null, array $headers = []): array
    {
        if (null === $version) {
            $version = $this->configurationLoader->getDefaultVersion($serviceName);
        }

        $client = $this->getClient($serviceName, $version, $headers);
        $response = $client->query($query, $variables);
        $result = [
            'data' => $response->getData(),
        ];
        if (count($response->getErrors())) {
            $result['errors'] = $response->getErrors();
        }

        return $result;
    }

    public function forward(string $serviceName, string $version, string $rest, Request $request, array $headers = []): Response
    {
        switch ($request->getContentType()) {
            case 'form':
                $query = $request->request->get('query');
                $variables = $request->request->has('variables')
                    ? json_decode($request->request->get('variables'), true)
                    : []
                ;
                break;
            default:
                $payload = json_decode($request->getContent(), true);

                $query = $payload['query'];
                $variables = $payload['variables'];
                break;
        }

        $result = $this->query($serviceName, $query, $variables, $version, $headers);

        return new JsonResponse($result);
    }

    private function getClient(string $serviceName, string $version, array $headers = []): Client
    {
        $endpoint = $this->configurationLoader->getEndpoint($serviceName, $version, self::TYPE);

        return $this->clientBuilder->build($endpoint, $headers);
    }

    public function getException(string $code, string $message = ''): \Exception
    {
        return new UserError($code, $message);
    }

    public function generateErrorResponse(\Exception $exception): Response
    {
        $error = FormattedError::createFromException($exception, DebugFlag::INCLUDE_DEBUG_MESSAGE);

        return new JsonResponse([
            'errors' => [$error],
        ]);
    }
}
