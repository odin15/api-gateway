<?php

namespace App\Gateway\Forward;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface ForwardInterface
{
    public function forward(string $serviceName, string $version, string $rest, Request $request, array $headers = []): Response;

    public function getType(): string;

    public function getException(string $code, string $message = ''): \Exception;

    public function generateErrorResponse(\Exception $exception): Response;
}
