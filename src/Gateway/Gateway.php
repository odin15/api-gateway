<?php

namespace App\Gateway;

use App\Gateway\Forward\ForwardStrategy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Gateway
{
    public function __construct(private ForwardStrategy $forwardStrategy)
    {
    }

    public function forwardRequest(string $serviceName, string $version, Request $request, string $rest): Response
    {
        return $this->forwardStrategy->forward($serviceName, $version, $request, $rest);
    }
}
