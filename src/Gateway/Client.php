<?php

namespace App\Gateway;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\HttpOptions;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Client
{
    protected HttpClientInterface $httpClient;

    public function __construct(
        protected string $endpoint,
        protected array $headers = [],
        protected ?string $token = null
    ) {
        $options = (new HttpOptions())
            ->setHeaders(array_merge($headers, [
                'Content-Type' => 'application/json',
                'User-Agent' => 'Odeven GraphQL client',
            ]))
        ;

        if (null !== $this->token) {
            $options->setAuthBearer($this->token);
        }

        $this->httpClient = HttpClient::createForBaseUri($endpoint, $options->toArray());
    }

    public function setHttpClient(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function query(string $query, array $variables = [], array $files = [])
    {
        $options = new HttpOptions();
        $dataParts = [
            'query' => new DataPart($query, null, 'text/plain'),
            'variables' => new DataPart(json_encode($variables), null, 'application/json'),
        ];

        if (count($files)) {
            foreach ($files as $name => $filePath) {
                $dataParts[$name] = DataPart::fromPath($filePath);
            }
        }

        $formDataPart = new FormDataPart($dataParts);
        $options->setHeaders($formDataPart->getPreparedHeaders()->toArray());
        $options->setBody($formDataPart->bodyToIterable());

        $response = $this->httpClient->request('POST', $this->endpoint, $options->toArray());

        return $this->contentToResponse($response->getContent());
    }

    private function contentToResponse(string $body): Response
    {
        $decodedResponse = $this->getJsonDecodedResponse($body);

        if (false === array_key_exists('data', $decodedResponse) && empty($decodedResponse['errors'])) {
            throw new \UnexpectedValueException('Invalid GraphQL JSON response. Response body: '.json_encode($decodedResponse));
        }

        return new Response(
            $decodedResponse['data'] ?? [],
            $decodedResponse['errors'] ?? [],
        );
    }

    private function getJsonDecodedResponse(string $body)
    {
        $response = json_decode($body, true);

        $error = json_last_error();
        if (JSON_ERROR_NONE !== $error) {
            throw new \UnexpectedValueException('Invalid JSON response. Response body: '.$body);
        }

        return $response;
    }
}
