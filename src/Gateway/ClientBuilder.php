<?php

namespace App\Gateway;

class ClientBuilder
{
    public function build(string $endpoint, array $headers = [], ?string $token = null): Client
    {
        return new Client($endpoint, $headers, $token);
    }
}
