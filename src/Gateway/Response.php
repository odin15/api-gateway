<?php

namespace App\Gateway;

class Response
{
    public function __construct(private array $data = [], private array $errors = [])
    {
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }
}
