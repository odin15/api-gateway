Api-Gateway Service
============

This service provide a single API endpoint to access odin micro-services.

Installation
============

### Option 1 (recommanded) : Docker Compose

See full configuration on [GitLab](https://gitlab.dyosis.com/odin/odin/-/tree/master)

### Option 2 : Docker

_This service require a MySQL server, an AMQP server, and an Odin's API-Gateway service to work._

Here is a working example using 1 networks :
* odin : enable odin micro-services to communicate

```console
docker create \
  --name api-gateway.odin \
  --network=odin \
  -e APP_ENV='prod' \
  -e CORS_ALLOW_ORIGIN='^.*$' \
  -e MICROSERVICE_ACCESS_TOKEN='ThisTokenIsNotSecretChangeIt' \
  -e MICROSERVICE_CUSTOMER_GRAPHQL_URL='http://localhost/odin/customer/public/' \
  -e MICROSERVICE_CALENDAR_GRAPHQL_URL='http://localhost/odin/calendar/public/' \
  -e MICROSERVICE_BOOKEO_GRAPHQL_URL='http://localhost/odin/bookeo/public/' \
  registry.gitlab.odeven.fr/odin/api-gateway:master \
&& docker start api-gateway.odin
```

Configuration
=============

### Environment variables

* `CORS_ALLOW_ORIGIN` : CORS rules (`*` to allow every origin)
* `MICROSERVICE_ACCESS_TOKEN` : Token to authenticate as a service on Odin
* `MICROSERVICE_CUSTOMER_GRAPHQL_URL` : URL to access Odin's `Customer` micro-service GraphQL API
* `MICROSERVICE_CALENDAR_GRAPHQL_URL` : URL to access Odin's `Calendar` micro-service GraphQL API
* `MICROSERVICE_BOOKEO_GRAPHQL_URL` : URL to access Odin's `Bookeo` micro-service GraphQL API