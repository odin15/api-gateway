#!/bin/bash

# Usage
# $ chmod +x ./wait-for-http.sh
# $ sh wait-for-http.sh http://<host>:<port>/<geometry>

echoerr() {
  echo "$@" 1>&2;
}

echoerr "Waiting for ${1}"
i=0
while [[ "$(curl -s -o /dev/null -w '%{http_code}' ${1})" != "200" ]]; do
	sleep 5
	let "i+=5"
done

echoerr "${1} is available after ${i} seconds"
