#!/bin/bash

apacheEnvVarsStr=""

while read envVar; do
  [[ "$envVar" =~ ([A-Z0-8_]+)= ]]
  envVarName="${BASH_REMATCH[1]}"
  if [ -n "$envVarName" ]; then
    apacheEnvVarsStr+="SetEnv $envVarName \${$envVarName}"
    apacheEnvVarsStr+="\\\n    "
  fi
done < ./.env

sed -i "s/__ENV_VARS__/$(printf %b "$apacheEnvVarsStr")/" /etc/apache2/sites-available/localhost.conf
