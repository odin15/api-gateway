#!/bin/bash

# Dump env variables to apache config
./.docker/build/scripts/dump-env-vars.sh

# Wait until MySQL Server is ready
echo 'Waiting for MySQL...'
./.docker/build/scripts/wait-for-it.sh ${MYSQL_HOST}:${MYSQL_PORT} --timeout=120

# Create/Update Database
echo 'Database Migration...'
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:migrations:migrate --no-interaction

# Wait until AMQP Server is ready
echo 'Waiting for AMQP...'
./.docker/build/scripts/wait-for-it.sh ${AMQP_HOST}:${AMQP_PORT} --timeout=120

# Create default user
if [[ -n $DEFAULT_USER_EMAIL ]]
then
  if [[ -n $DEFAULT_USER_PASSWORD ]]
  then
    php bin/console app:user:create "${DEFAULT_USER_EMAIL}" -r "ROLE_ADMIN" -p "${DEFAULT_USER_PASSWORD}"
  else
    php bin/console app:user:create "${DEFAULT_USER_EMAIL}" -r "ROLE_ADMIN"
  fi
fi

/usr/bin/supervisord -c /var/www/.docker/build/conf/supervisord/supervisord.conf
